package main

import "fmt"

//User es la Clase tipo Usuario
type User struct {
	name        string
	age         int
	bankBanlace float32
	RoleDetail RoleDetail
}

//Estructuras anidadas

//RoleDetail ejemplo para definir etructuras anidadas
type RoleDetail struct {
	postion string
	team    string
}

func main() {

	roleDetailUser1 := RoleDetail{
		postion: "Software Engineer",
		team: "Transport",
	}

	user1 := User{
		name:        "Jean Pierre",
		age:         28,
		bankBanlace: 20000,
		RoleDetail: roleDetailUser1,
	}

	fmt.Println(user1.name)
	fmt.Println(user1.age)
	fmt.Println(user1.RoleDetail)
	fmt.Println(user1.RoleDetail.postion)

	fmt.Println(user1)
}
