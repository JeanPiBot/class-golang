package main

import "fmt"


func main()  {
	lookup := make(map[string]int)
	lookup["Goku"] = 9001
	lookup["Vegeta"] = 9000

	power, exists := lookup["Vegeta"]

	fmt.Println(power, exists)

	power, exists = lookup["Goku"]

	fmt.Println(power, exists)

	total := len(lookup)

	fmt.Println(total)

	fmt.Println(lookup)

	delete(lookup, "Vegeta")

	fmt.Println(lookup)

	for key, value := range lookup {
		fmt.Printf("el key es %s y su valor es %d\n", key, value)
	}

}