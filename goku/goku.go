package main

import "fmt"

//Person is a someone
type Person struct {
	name string
}

//Introduce is a method which present a person
func (p *Person) Introduce() {
	fmt.Printf("Hi, my name is %s\n", p.name)
}

//Sayayin is a power man
type Sayayin struct {
	*Person
	power int
}

//SuperSayayin is a change of person
func (s *Sayayin) SuperSayayin() {
	s.power += 10000
	fmt.Printf("My power is %d\n", s.power)
}

func main()  {
	goku := &Sayayin{
		Person: &Person{"Goku"},
		power: 9001,
	}

	goku.Introduce()

	goku.SuperSayayin()

}