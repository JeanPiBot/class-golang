package main

import "fmt"

//Peliculas es la clase de peliculas
type Peliculas struct{
	titulo string
	director string
	protagonistas string
	año int
}

func main()  {
	var pelis[2] Peliculas

	pelis[0].titulo = "Spree"
	pelis[0].director = "Eugene Kotlyarenko"
	pelis[0].protagonistas = "Joe Keery, Sasheer Zamata, David Arquette"
	pelis[0].año = 2020

	pelis[1].titulo = "The New Mutants"
	pelis[1].director = "Josh Boone"
	pelis[1].protagonistas = "Maisie Williams, Anya Taylor-Joy, Charlie Heaton"
	pelis[1].año = 2020

	fmt.Printf("Nombre\t\tDirector\t\tProtagonistas\t\t\t\t\t\tAño\n")
	fmt.Printf("%s\t\t", pelis[0].titulo)
	fmt.Printf("%s\t", pelis[0].director)
	fmt.Printf("%s\t\t", pelis[0].protagonistas)
	fmt.Printf("%d\n", pelis[0].año)
	fmt.Printf("%s\t", pelis[1].titulo)
	fmt.Printf("%s\t\t", pelis[1].director)
	fmt.Printf("%s\t", pelis[1].protagonistas)
	fmt.Printf("%d\n", pelis[1].año)

}