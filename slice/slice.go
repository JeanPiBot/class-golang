package main

import (
	"fmt"
)

func main() {
	scores := make([]int, 0, 5)

	c := cap(scores)

	fmt.Println(c)

	names := []string{"Jean Pierre", "Maria", "Anna", "Gina", "Pedro"}

	fmt.Println(names)

	for valor := range names {
		fmt.Printf("Mi nombre es %s\n", names[valor])
	}

	for i := 0; i < 25; i++ {
		scores = append(scores, i)

		if cap(scores) != c {
			c = cap(scores)
			fmt.Println(c)
		}
	}

	fmt.Printf("scores are %d\n", scores)
}
